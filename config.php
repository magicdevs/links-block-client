<?php
/**
 * Файл содержит настройки.
 *
 * PHP версия 5
 *
 */

// путь к папке с кешем (необходимы права на запись)
define("LINKS_BLOCK_CACHE_PATH",__DIR__."/cache");

// уникальный ключ которым мы предоставляем вам единоразово для синхронизации с нашим сервером
define("LINKS_BLOCK_KEY","YOUR_KEY_HERE");





// отключает кэширование, используется во время тестирования !крайне не желательно устанавливать в true!
define("LINKS_BLOCK_DISABLE_CACHE",true);

// запрос для подключения к нашему серверу !не стоит менять!
define("LINKS_BLOCK_DATA_URL","http://api-links.seoshield.ru/links_block-manager.php?key=".md5(LINKS_BLOCK_KEY));
